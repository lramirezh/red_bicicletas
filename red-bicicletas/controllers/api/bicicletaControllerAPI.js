var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo)
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);
    res.status(200).json({Bicicleta: bici});
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(
        req.body.code,
        function(error, biciDelete){
            res.status(204).send();
        }
    );
}

exports.bicicleta_update = function(req, res){
    Bicicleta.findOneAndUpdate(
        {"code": req.body.code},
        {
            "code": req.body.code,
            "color": req.body.color,
            "modelo": req.body.modelo,
            "ubicacion": [req.body.lat, req.body.lng]
        },
        function(error, biciUpdate){
            res.status(200).json({
                Bicicleta: biciUpdate
            });
        }
    );
}