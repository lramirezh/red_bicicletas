var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

/* Testing Instancias del modelo Mongo */

describe('Testing Bicicletas', function(){
    beforeEach(function(done){

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error de conexión'));
        db.once('open', function() {
            console.log('Estas conectado a la base de datos Test!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-33.5, -77.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toEqual(-33.5);
            expect(bici.ubicacion[1]).toEqual(-77.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Colección comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "Roja", modelo: "Urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Debe eliminar y dejar en limpia la colección bicicletas', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    Bicicleta.removeByCode(1, function(err, targetBici){
                        if(err) console.log(err);
                        Bicicleta.allBicis(function(err, bicis){
                            expect(bicis.length).toEqual(0);
                            done();
                        });
                    });
                });
            });
        });
    });

});
