var Reserva = require('../../models/reserva');
var mongoose = require('mongoose');
const Usuario = require('../../models/usuario');
const Bicicleta = require('../../models/bicicleta');

describe('Testing Usuarios', function(){

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
        mongoose.set('useFindAndModify', false);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error de conexión'));
        db.once('open', function() {
            console.log('Estas conectado a la base de datos Test!')
            done();
        });
    });

    /* Borra datos de entidades del modelo de datos en cascada "tradicional" */
    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    /* Testing de una reserva */
    describe('Cuando un Usuario reserva una bici', () => {
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Luis'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });



});