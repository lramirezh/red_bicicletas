var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error de conexión'));
        db.once('open', function() {
            console.log('Estas conectado a la base de datos Test!');
            done();
        });

    });

   afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                if(error) console.log(error);
                var result = JSON.parse(body);
                console.log(Bicicleta.result);
                Bicicleta.allBicis(function(err, bicis){
                    expect(response.statusCode).toBe(200);
                    expect(bicis.length).toEqual(0);
                    done();
                });
            });
        });
    });

    describe('POST BICICLETAS /create', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "Rojo", "modelo": "Urbana", "lat": -33, "lng": -70}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici,
            },
            function(error, response, body) {
                if(error) console.log(error);
                var bici = JSON.parse(body);
                console.log(bici);
                expect(response.statusCode).toBe(200);
                expect(bici.Bicicleta.color).toBe("Rojo");
                expect(bici.Bicicleta.ubicacion[0]).toBe(-33);
                expect(bici.Bicicleta.ubicacion[1]).toBe(-70);
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('STATUS 200', (done) => {
            var aBici = new Bicicleta({code: 7, color: "Amarillo", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
            });

            var headers = {'content-type' : 'application/json'};
            var bBici = '{ "code": 7, "color": "Azul", "modelo": "Urbana", "lat": -33, "lng": -70}';
            request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: bBici,
            },
            function(error, response, body){
                if(error) console.log(error);
                var bici = JSON.parse(body);
                console.log(bici);
                Bicicleta.findByCode(7, function(error, targetBici){
                    if(error) console.log(error);
                    expect(response.statusCode).toBe(200);
                    expect(targetBici.color).toBe("Azul");
                    done();
                }); 
            });
        });
    });


    describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 204', (done) => {
            var a = new Bicicleta({code: 7, color: "Amarillo", modelo: "Urbana"});
            Bicicleta.add(a); 

            var b = new Bicicleta({code: 3, color: "Amarillo", modelo: "Urbana"});
            Bicicleta.add(b);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 7}';
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici 
                },
                function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(Bicicleta.allBicis.length).toBe(1);
                    done();
                }
            );
        });
    });

});